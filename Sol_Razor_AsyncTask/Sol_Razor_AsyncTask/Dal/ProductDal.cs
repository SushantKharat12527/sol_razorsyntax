﻿using Sol_Razor_AsyncTask.Dal.ORD;
using Sol_Razor_AsyncTask.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sol_Razor_AsyncTask.Dal
{
    public class ProductDal
    {
        #region Declaration
        private ProductDCDataContext _dc = null;
        #endregion

        #region ConStructor
        public ProductDal()
        {
            _dc = new ProductDCDataContext();
        }
        #endregion

        public IEnumerable<ProductEntity> GetPersonData()
        {
            var getQuery =
                    _dc
                    ?.tblProducts
                    ?.AsEnumerable()
                    ?.Select((leLinqPersonObj) => new ProductEntity()
                    {
                        ProductId = leLinqPersonObj.Product_Id,
                        ProductName = leLinqPersonObj.Product_Name,
                        ProductPrice = leLinqPersonObj.Product_Price,
                        ProductQuantity=leLinqPersonObj.PRoduct_AvailableQuantity
                    })
                    ?.Take(5)
                    ?.ToList();

            return getQuery;
        }
    }
}