﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sol_Razor_AsyncTask.Entity
{
    public class ProductEntity
    {
        public decimal ProductId { get; set; }

        public String ProductName { get; set; }

        public decimal ProductPrice { get; set; }

        public decimal ProductQuantity { get; set; }
    }
}